package com.luv2code.springdemo;

public class TrackCoach implements Coach {

    private FortuneService fortuneService;

    public TrackCoach(){

    }

    public TrackCoach(FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "You have run a hard 5K";
    }

    @Override
    public String getDailyFortune() {
        return "Just Do It: "+ fortuneService.getFortune();
    }

    // add an init method
    public void doMyStartupStuff(){
        System.out.println("TrackCoach: inside method doMyStartupStuff");
    }

    // add a destroy method
    public void doMyCleanStuffYoYO(){
        System.out.println("TrackCoach: inside method doMyCleanStuffYoYO");
    }
}
